import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        Imprime variavel = new Imprime();
        Double scanner = new Double();
        boolean aprovacao = false;
        double primeiraNota, segundaNota, terceiraNota, quartaNota, recuperacaoNota, media;

        variavel.imprime("Verificador de notas\n");

        variavel.imprime("Digite a primeira nota:");
        primeiraNota = scanner.capturaDouble(entrada);

        variavel.imprime("Digite a segunda nota:");
        segundaNota = scanner.capturaDouble(entrada);

        variavel.imprime("Digite a terceira nota:");
        terceiraNota = scanner.capturaDouble(entrada);

        variavel.imprime("Digite a quarta nota: ");
        quartaNota = scanner.capturaDouble(entrada);

        Media calculaMedia = new Media();

        calculaMedia.calcula(primeiraNota,segundaNota,terceiraNota,quartaNota);
        media = calculaMedia.calcula(primeiraNota,segundaNota,terceiraNota,quartaNota);

        if(media >= 7) aprovacao = true;
        else{
            variavel.imprime("O aluno ficou de recuperação, digite a nota: ");
            recuperacaoNota = entrada.nextDouble();
            media = (media + recuperacaoNota)/2;
            if(media >= 7) aprovacao = true;
        }

        if(aprovacao == true){
            variavel.imprime("\n\nO Aluno foi aprovado com a média "+media);
        }else{
            variavel.imprime("\n\nO Aluno foi reprovado com a média "+media);
        }

    }

}